const lintStagedConfig = {
	'*.{js,mjs,cjs,jsx,ts,mts,cts,tsx}': [
		'eslint --fix',
		'prettier --check --write',
	],
	'!*.{js,mjs,cjs,jsx,ts,mts,cts,tsx}':
		'prettier --check --write --ignore-unknown',
};

export default lintStagedConfig;
