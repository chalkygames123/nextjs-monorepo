// @ts-check

/**
 *  @type { import('next').NextConfig }
 */
const nextConfig = {
	// Disabled as React Spectrum doesn't yet support it: https://github.com/adobe/react-spectrum/issues/779
	reactStrictMode: false,

	swcMinify: true,
};

export default nextConfig;
