// @ts-check

/**
 * @type { import('tailwindcss').Config }
 */
const tailwindConfig = {
	content: ['./pages/**/*.{ts,tsx}', './components/**/*.{ts,tsx}'],
	theme: {
		extend: {},
	},
	plugins: [],
};

module.exports = tailwindConfig;
