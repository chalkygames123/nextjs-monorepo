import type { NextPage } from 'next';
import Head from 'next/head';

const Home: NextPage = () => (
	<>
		<Head>
			<title>Home</title>
		</Head>
		<main>
			<p className="text-red-500">Hello, world!</p>
		</main>
	</>
);

export default Home;
