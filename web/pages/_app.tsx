import type { AppProps } from 'next/app';
import { SSRProvider } from 'react-aria';
import '../styles/globals.css';

export default function MyApp({ Component, pageProps }: AppProps) {
	return (
		<SSRProvider>
			<Component {...pageProps} />
		</SSRProvider>
	);
}
