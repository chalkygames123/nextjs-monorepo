const { join } = require('node:path');

const commonRules = {
	'capitalized-comments': 'off',
	'class-methods-use-this': 'error',
	'unicorn/prevent-abbreviations': [
		'error',
		{
			replacements: {
				props: false,
				ref: false,
			},
		},
	],
};

/**
 *  @type { import('eslint').Linter.Config }
 */
module.exports = {
	root: true,
	extends: [
		'plugin:unicorn/recommended',
		'xo',
		// Workaround to suppress warning "The Next.js plugin was not detected in your ESLint configuration."
		'next/core-web-vitals',
		'prettier',
	],
	rules: {
		...commonRules,
	},
	settings: {
		next: {
			rootDir: 'web',
		},
		tailwindcss: {
			config: 'web/tailwind.config.cjs',
		},
	},
	overrides: [
		{
			files: '*.{mjs,cjs,jsx}',
		},
		{
			files: '*.{ts,mts,cts,tsx}',
			extends: [
				'plugin:unicorn/recommended',
				'plugin:tailwindcss/recommended',
				'plugin:@typescript-eslint/recommended',
				'plugin:@typescript-eslint/recommended-requiring-type-checking',
				'plugin:@typescript-eslint/strict',
				'xo/browser',
				'xo-typescript',
				'next/core-web-vitals',
				'prettier',
			],
			parserOptions: {
				tsconfigRootDir: join(__dirname, 'web'),
			},
			rules: {
				...commonRules,
			},
		},
	],
};
